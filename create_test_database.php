<?php
require_once __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$servername = $_ENV['MYSQL_HOST'] ?: 'localhost';
$username = $_ENV['MYSQL_USER'] ?: 'root';
$password = $_ENV['MYSQL_PASSWORD'] ?: '';
$dbname = $_ENV['MYSQL_TEST_DATABASE'] ?: 'to_do_list_test';

try {
    $pdo = new PDO("mysql:host=$servername", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "CREATE DATABASE IF NOT EXISTS $dbname";
    $pdo->exec($sql);

    $sql = file_get_contents('setup_test_db.sql');
    $pdo->exec("USE $dbname");
    $pdo->exec($sql);

    echo "Test database setup successfully.\n";
} catch (PDOException $e) {
    die("Could not connect to the database $servername :" . $e->getMessage());
}
?>
