<?php
require 'task_functions.php';

use PHPUnit\Framework\TestCase;

class TaskFunctionsTest extends TestCase {
    private $conn;
    private $stmt;

    protected function setUp(): void {
        $this->conn = $this->createMock(PDO::class);
        $this->stmt = $this->createMock(PDOStatement::class);
        $this->conn->method('prepare')->willReturn($this->stmt);
    }
    
    //ADD TASKS FUNCTION
    public function testAddTaskEmptyTitle() {
        $result = addTask(1, "", $this->conn);
        $this->assertEquals("Le titre de la tâche ne peut pas être vide.", $result);
    }

    public function testAddTaskAlreadyExists() {
        $this->stmt->method('fetch')->willReturn(true);
        $result = addTask(1, "Test Task", $this->conn);
        $this->assertEquals("Une tâche avec ce titre existe déjà.", $result);
    }

    public function testAddTaskSuccess() {
        $this->stmt->method('fetch')->willReturn(false);
        $this->stmt->method('execute')->willReturn(true);
        $result = addTask(1, "New Task", $this->conn);
        $this->assertEquals("success", $result);
    }

    //DELETE TASKS FUNCTION
    public function testDeleteTaskSuccess() {
        $this->stmt->method('execute')->willReturn(true);
        $result = deleteTask(1, $this->conn);
        $this->assertEquals("success", $result);
    }

    public function testDeleteTaskFail() {
        $this->stmt->method('execute')->willReturn(false);
        $result = deleteTask(1, $this->conn);
        $this->assertEquals("Erreur lors de la suppression de la tâche.", $result);
    }

    //TOGGLE TASKS FUNCTION
    public function testToggleTaskStatusSuccess() {
        $this->stmt->method('fetch')->willReturn(['checked' => 0]);
        $this->stmt->method('execute')->willReturn(true);
        $result = toggleTaskStatus(1, $this->conn);
        $this->assertEquals("success", $result);
    }

    public function testToggleTaskStatusFail() {
        $this->stmt->method('fetch')->willReturn(false);
        $result = toggleTaskStatus(1, $this->conn);
        $this->assertEquals("Erreur lors de la modification du statut de la tâche.", $result);
    }

    //GET USER TASKS FUNCTION
    public function testGetTasksByUser() {
        $this->stmt->method('fetchAll')->willReturn([]);
        $result = getTasksByUser(1, $this->conn);
        $this->assertEquals([], $result);
    }


}

