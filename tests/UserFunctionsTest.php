<?php
require 'user_functions.php';
use PHPUnit\Framework\TestCase;

class UserFunctionsTest extends TestCase {
    private $conn;
    private $stmt;

    protected function setUp(): void {
        $this->conn = $this->createMock(PDO::class);
        $this->stmt = $this->createMock(PDOStatement::class);
        $this->conn->method('prepare')->willReturn($this->stmt);
    }

    public function testRegisterUserAlreadyExists() {
        $username = "existingUser";
        $password = "Password123!";
        $this->stmt->method('fetch')->willReturn(true); // Simulate user exists

        $result = registerUser($username, $password, $this->conn);
        $this->assertEquals("Ce nom d'utilisateur est déjà pris. Veuillez en choisir un autre.", $result);
    }

    public function testRegisterUserSuccess() {
        $username = "newUser";
        $password = "Password123!";
        $this->stmt->method('fetch')->willReturn(false); // No user exists
        $this->stmt->method('execute')->willReturn(true); // Simulate successful insert

        $result = registerUser($username, $password, $this->conn);
        $this->assertEquals("success", $result);
    }

    public function testLoginUserSuccess() {
        $username = "testuser";
        $password = "validPassword123!";
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        $this->stmt->method('fetch')->willReturn(['id' => 1, 'password' => $hashedPassword]);

        $result = loginUser($username, $password, $this->conn);
        $this->assertEquals('success', $result);
    }

    public function testLoginUserFail() {
        $username = "testuser";
        $password = "wrongPassword";
        $this->stmt->method('fetch')->willReturn(['id' => 1, 'password' => password_hash("anotherPassword", PASSWORD_DEFAULT)]);

        $result = loginUser($username, $password, $this->conn);
        $this->assertEquals('Identifiants ou Mot de passe incorrects.', $result);
    }


    public function testIsPasswordStrong() {
        $weakPassword = "short";
        $strongPassword = "StrongPassword1!";

        $this->assertFalse(isPasswordStrong($weakPassword));
        $this->assertTrue(isPasswordStrong($strongPassword));
    }
}

