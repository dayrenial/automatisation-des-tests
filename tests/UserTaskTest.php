<?php
use PHPUnit\Framework\TestCase;

class UserTaskTest extends TestCase {
    private $pdo;

    protected function setUp(): void
    {
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '.env.test');
        $dotenv->load();

        $dbHost = getenv('MYSQL_HOST') ?: 'localhost';
        $dbName = getenv('MYSQL_TEST_DATABASE') ?: 'to_do_list_test';
        $dbUser = getenv('MYSQL_USER') ?: 'root';
        $dbPass = getenv('MYSQL_PASSWORD') ?: '';

        $this->pdo = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUser, $dbPass);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function testUserRegistrationAndLogin()
    {
        // Test de l'inscription
        $username = "testUser";
        $password = "testPass123!";
        registerUser($username, $password, $this->pdo);
        $query = $this->pdo->query("SELECT * FROM users WHERE username = '$username'");
        $result = $query->fetch();
        $this->assertNotEmpty($result);

        // Test de la connexion
        $loginResult = loginUser($username, $password, $this->pdo);
        $this->assertEquals('success', $loginResult);
    }

    public function testTaskOperations()
    {
        // Création d'un utilisateur pour le test
        $username = "testUser2";
        $password = "testPass123!";
        $result = registerUser($username, $password, $this->pdo);
        $this->assertEquals('success', $result, "User registration failed: $result");

        $userId = $this->pdo->lastInsertId();

        // Vérification que l'utilisateur a bien été inséré
        $query = $this->pdo->query("SELECT * FROM users WHERE id = '$userId'");
        $user = $query->fetch();
        $this->assertNotEmpty($user, "User insertion failed.");

        // Ajout d'une tâche
        $title = "New Task";
        $result = addTask($userId, $title, $this->pdo);
        $this->assertEquals('success', $result, "Task insertion failed: $result");

        $query = $this->pdo->query("SELECT * FROM todos WHERE user_id = '$userId'");
        $tasks = $query->fetchAll();
        $this->assertEquals(1, count($tasks), "Task count mismatch.");

        // Modification de la tâche
        $taskId = $tasks[0]['id'];
        $result = toggleTaskStatus($taskId, $this->pdo);
        $this->assertEquals('success', $result, "Task status toggle failed: $result");

        $query = $this->pdo->query("SELECT * FROM todos WHERE id = '$taskId'");
        $updatedTask = $query->fetch();
        $this->assertEquals(1, $updatedTask['checked'], "Task update failed.");

        // Suppression de la tâche
        deleteTask($taskId, $this->pdo);
        $query = $this->pdo->query("SELECT * FROM todos WHERE id = '$taskId'");
        $deletedTask = $query->fetch();
        $this->assertFalse($deletedTask, "Task deletion failed.");

        deleteUserAndTasks($userId, $this->pdo);
        $userCheck = $this->pdo->query("SELECT * FROM users WHERE id = '$userId'")->fetch();
        $tasksCheck = $this->pdo->query("SELECT * FROM todos WHERE user_id = '$userId'")->fetchAll();
        $this->assertFalse($userCheck, "User deletion failed.");
        $this->assertEmpty($tasksCheck, "Tasks deletion failed.");
    }




}
