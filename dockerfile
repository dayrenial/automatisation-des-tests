# Utiliser une image PHP officielle avec Apache
FROM php:7.4-apache

# Installer les extensions PHP nécessaires et le client MariaDB
RUN apt-get update && apt-get install -y \
    libzip-dev \
    unzip \
    git \
    default-mysql-client \
    curl \
    && docker-php-ext-install zip pdo pdo_mysql

# Installer Node.js version 16
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash - \
    && apt-get install -y nodejs

# Créer un répertoire de cache Cypress avec les bonnes permissions
RUN mkdir -p /var/www/html/cypress_cache && chown -R www-data:www-data /var/www/html/cypress_cache
ENV CYPRESS_CACHE_FOLDER=/var/www/html/cypress_cache

# Installer Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Copier les fichiers de l'application
COPY . /var/www/html/

# Installer les dépendances PHP
RUN composer install

# Installer les dépendances npm
RUN npm install
