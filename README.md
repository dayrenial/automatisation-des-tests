# Projet de Gestion de Tâches

Ce projet est une application web de gestion de tâches simple qui permet aux utilisateurs de créer, de visualiser, de modifier et de supprimer des tâches quotidiennes. Elle inclut également des fonctionnalités de connexion et d'inscription pour que chaque utilisateur puisse gérer ses propres tâches de manière sécurisée

## Fonctionnalités

- **Inscription des utilisateurs** : Permet aux nouveaux utilisateurs de créer un compte.
- **Connexion des utilisateurs** : Permet aux utilisateurs existants de se connecter à l'application.
- **Ajout de tâches** : Les utilisateurs peuvent ajouter de nouvelles tâches à leur liste.
- **Visualisation des tâches** : Affiche toutes les tâches enregistrées par l'utilisateur.
- **Modification du statut des tâches** : Les utilisateurs peuvent marquer les tâches comme terminées ou les remettre en état non terminé.
- **Suppression des tâches** : Permet la suppression des tâches.

## Technologies utilisées

- **Front-end** : HTML, CSS
- **Back-end** : PHP
- **Base de données** : MySQL
- **Gestion des sessions** : PHP Sessions
- **Gestion des environnements** : Dotenv pour la gestion des variables d'environnement

## Configuration et installation

### Prérequis

- PHP 7.4 ou supérieur
- MySQL
- Serveur web comme Apache ou Nginx
- Composer pour la gestion des dépendances PHP

### Installation

1. **Cloner le dépôt**


2. **Installer les dépendances PHP**

composer require vlucas/phpdotenv

composer require --dev phpunit/phpunit ^9

composer require --dev php-mock/php-mock-phpunit


3. **Configurer l'environnement**

Copiez le fichier `.env.example` en `.env` et modifiez les variables d'environnement pour correspondre à votre configuration de base de données et autres paramètres secrets.


4. **Initialiser la base de données**

Importez le fichier `schema.sql` dans votre système de gestion de base de données pour créer les tables nécessaires.

