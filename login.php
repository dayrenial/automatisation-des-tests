<?php 
require 'user_functions.php';
session_start();

$error = '';
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['username'], $_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $result = loginUser($username, $password, $conn);
    if ($result == "success") {
        header("Location: index.php");
        exit;
    } else {
        $error = $result;
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
    <link rel="stylesheet" href="css/style.css"> <!-- Assurez-vous que le chemin d'accès est correct -->
</head>
<body>
    <div class="main-section">
        <div class="add-section">
            <h1 style="text-align:center; padding: 20px 0;">Connexion</h1>
            <form action="login.php" method="post">
                Nom d'utilisateur: <input type="text" name="username" required>
                Mot de passe: <input type="password" name="password" required>
                <p style="color: red;"><?= $error ?></p>
                <button type="submit">Se connecter</button>
            </form>
            <p style="text-align: center;">Pas encore de compte? <a href="register.php">Inscrivez-vous ici</a>.</p>
        </div>
    </div>
</body>
</html>
