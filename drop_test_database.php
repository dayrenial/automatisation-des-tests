<?php
require_once __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$servername = $_ENV['MYSQL_HOST'] ?: 'localhost';
$username = $_ENV['MYSQL_USER'] ?: 'root';
$password = $_ENV['MYSQL_PASSWORD'] ?: '';

// Connexion à la base de données MySQL
$pdo = new PDO("mysql:host=$servername", $username, $password);

// Suppression de la base de données de test
$pdo->exec("DROP DATABASE IF EXISTS " . ($_ENV['MYSQL_DATABASE'] ?: 'to_do_list_test'));

echo "Database '" . ($_ENV['MYSQL_DATABASE'] ?: 'to_do_list_test') . "' dropped successfully.\n";
