<?php
require_once 'db_conn.php';

function addTask($userId, $title, $conn)
{
    if (empty($title)) {
        return "Le titre de la tâche ne peut pas être vide.";
    }

    $stmt = $conn->prepare("SELECT id FROM todos WHERE title = ? AND user_id = ?");
    $stmt->execute([$title, $userId]);
    if ($stmt->fetch()) {
        return "Une tâche avec ce titre existe déjà.";
    }

    $stmt = $conn->prepare("INSERT INTO todos (title, user_id) VALUES (?, ?)");
    if ($stmt->execute([$title, $userId])) {
        return "success";
    } else {
        $errorInfo = $stmt->errorInfo();
        return "Erreur lors de l'ajout de la tâche: " . $errorInfo[2];
    }
}


function deleteTask($taskId,$conn) {
    
    $stmt = $conn->prepare("DELETE FROM todos WHERE id = ?");
    if ($stmt->execute([$taskId])) {
        return "success";
    }
    return "Erreur lors de la suppression de la tâche.";
}

function toggleTaskStatus($taskId,$conn) {

    $stmt = $conn->prepare("SELECT checked FROM todos WHERE id = ?");
    $stmt->execute([$taskId]);
    $todo = $stmt->fetch();
    if ($todo) {
        $newStatus = $todo['checked'] ? 0 : 1;
        $stmt = $conn->prepare("UPDATE todos SET checked = ? WHERE id = ?");
        if ($stmt->execute([$newStatus, $taskId])) {
            return "success";
        }
    }
    return "Erreur lors de la modification du statut de la tâche.";
}

function getTasksByUser($userId,$conn) {

    $stmt = $conn->prepare("SELECT * FROM todos WHERE user_id = ? ORDER BY id DESC");
    $stmt->execute([$userId]);
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}
