<?php 
require 'db_conn.php';
require 'user_functions.php';
require 'task_functions.php';
session_start();

if (!isUserAuthenticated()) {
    redirectToLogin();
}

$tasks = getTasksByUser($_SESSION['user_id'],$conn);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>To-Do List</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <!-- Ajout en haut de la page ou dans un menu -->
    <form action="logout.php" method="post" >
        <button type="submit" id="logoutButton" class="logout-icon"><svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" fill="currentColor" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0z"/>
  <path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708z"/>
</svg></button>
    </form>

    <div class="main-section">
       <div class="add-section">
       <form action="app/add.php" method="POST" autocomplete="off">
             <?php if(isset($_GET['mess']) && $_GET['mess'] == 'error'){ ?>
                <input type="text" 
                    id="add_task"
                     name="title" 
                     style="border-color: #ff6666"
                     placeholder="<?php echo isset($_GET['data']) ? htmlspecialchars(urldecode($_GET['data'])) : 'Ce champ est requis'; ?>" />
              <button type="submit">Ajouter &nbsp; <span>&#43;</span></button>

             <?php }else{ ?>
              <input type="text" 
                     name="title" 
                     placeholder="Que faut-il faire ?" />
              <button type="submit">Ajouter &nbsp; <span>&#43;</span></button>
             <?php } ?>
          </form>
       </div>
       <div class="show-todo-section">
            <?php if(!$tasks){ ?>
                <div class="todo-item">
                    <div class="empty">
                        <img src="img/f.png" width="100%" />
                        <img src="img/Ellipsis.gif" width="80px">
                    </div>
                </div>
            <?php } ?>

            <?php foreach($tasks as $task) { ?>
                <div class="todo-item">
                    <span id="<?php echo $task->id; ?>"
                          class="remove-to-do">x</span>
                    <?php if($task->checked){ ?> 
                        <input type="checkbox"
                               class="check-box"
                               data-todo-id ="<?php echo $task->id; ?>"
                               checked />
                        <h2 class="checked"><?php echo $task->title ?></h2>
                    <?php }else { ?>
                        <input type="checkbox"
                               data-todo-id ="<?php echo $task->id; ?>"
                               class="check-box" />
                        <h2><?php echo $task->title ?></h2>
                    <?php } ?>
                    <br>
                    <small>created: <?php echo $task->date_time ?></small> 
                </div>
            <?php } ?>
       </div>
       <div class="add-section">
            <form action="delete_account.php" method="post">
                <button type="submit" id="deleteAccount" style="background: #ff0045;" onclick="return confirm('Êtes-vous sûr de vouloir supprimer votre compte et toutes vos tâches ? Cette action est irréversible.');">Supprimer mon compte</button>
            </form>
        </div>
    </div>

    <script src="js/jquery-3.2.1.min.js"></script>

    <script>
        $(document).ready(function(){
            $('.remove-to-do').click(function(){
                const id = $(this).attr('id');
                
                $.post("app/remove.php", 
                      {
                          id: id
                      },
                      (data)  => {
                         if(data){
                             $(this).parent().hide(600);
                         }
                      }
                );
            });

            $(".check-box").click(function(e){
                const id = $(this).attr('data-todo-id');
                
                $.post('app/check.php', 
                      {
                          id: id
                      },
                      (data) => {
                          if(data != 'error'){
                              const h2 = $(this).next();
                              if(data === '1'){
                                  h2.removeClass('checked');
                              }else {
                                  h2.addClass('checked');
                              }
                          }
                      }
                );
            });
        });
    </script>
</body>
</html>