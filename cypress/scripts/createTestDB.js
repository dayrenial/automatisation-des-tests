const mysql = require('mysql2/promise');

async function createDatabase() {
  const connection = await mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: ''
  });

  await connection.query(`CREATE DATABASE IF NOT EXISTS to_do_list_test`);
  console.log('Test database created or already exists');
  connection.end();
}

createDatabase().catch(console.error);
