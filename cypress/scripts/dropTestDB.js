const mysql = require('mysql2/promise');

async function dropDatabase() {
  const connection = await mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: ''
  });

  await connection.query(`DROP DATABASE IF EXISTS to_do_list_test`);
  console.log('Test database dropped');
  connection.end();
}

dropDatabase().catch(console.error);
