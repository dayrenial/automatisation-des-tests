describe('Tests de flux utilisateur', () => {
  before(() => {
    cy.visit('/login.php');
  });

  it('Tente de se connecter avec un utilisateur inexistant et navigue vers l\'inscription', () => {
    cy.get('input[name="username"]').type('fakeuser');
    cy.get('input[name="password"]').type('fakepassword{enter}');
    cy.get('p').should('contain', 'Identifiants ou Mot de passe incorrects.');
    cy.get('a').contains('Inscrivez-vous ici').click();
  
    cy.url().should('include', '/register.php');
  });

  it('Tente de s\'inscrire avec un mot de passe faible', () => {
    cy.visit('/register.php');
    cy.get('input[name="username"]').type('newuser');
    cy.get('input[name="password"]').type('weak{enter}');
    cy.get('p').should('contain', 'Le mot de passe doit contenir au moins 8 caractères');
  });

  it('Inscrit un nouvel utilisateur avec un mot de passe robuste', () => {
    cy.visit('/register.php');
    cy.get('input[name="username"]').type('newuser');
    cy.get('input[name="password"]').clear().type('StrongPassword1!');
    cy.get('button').contains('S\'inscrire').click();
    cy.url().should('include', '/login.php');
  });

  it('Se connecte et crée des tâches', () => {
    cy.visit('/login.php');

    cy.get('input[name="username"]').type('newuser');
    cy.get('input[name="password"]').type('StrongPassword1!{enter}');
    cy.url().should('include', '/index.php');

    cy.get('input[name="title"]').type('Tâche 1{enter}');

    cy.get('input[name="title"]').type('Tâche 2{enter}');

    cy.get('input[name="title"]').type('Tâche 3{enter}');

    cy.get('.check-box').first().click();

    cy.get('.remove-to-do').eq(1).click();
  });

  it('Se déconnecte et vérifie la page de connexion', () => {
    cy.visit('/login.php');
    cy.get('input[name="username"]').type('newuser');
    cy.get('input[name="password"]').type('StrongPassword1!{enter}');
    cy.get('button[id="logoutButton"]').click();
    cy.url().should('include', '/login.php');
  });

  it('Se reconnecte et vérifie les tâches', () => {
    cy.visit('/login.php');

    cy.get('input[name="username"]').type('newuser');
    cy.get('input[name="password"]').type('StrongPassword1!{enter}');
    cy.get('input[name="title"]').type('Tâche 1{enter}');
    cy.get('input[id="add_task"]').should('have.attr', 'placeholder', 'Une tâche avec ce titre existe déjà.')
  });

  it('Vérifie que le user n\'existe déja', () => {
    cy.visit('/register.php');

    cy.get('input[name="username"]').type('newuser');
    cy.get('input[name="password"]').type('StrongPassword1!{enter}');
    cy.get('button').contains('S\'inscrire').click();
    cy.get('p').should('contain', 'Ce nom d\'utilisateur est déjà pris. Veuillez en choisir un autre.');
  });

  it('Supprime le compte utilisateur', () => {
    cy.visit('/login.php');

    cy.get('input[name="username"]').type('newuser');
    cy.get('input[name="password"]').type('StrongPassword1!{enter}');

    cy.get('#deleteAccount').click();
    cy.url().should('include', '/login.php');

    cy.get('input[name="username"]').type('newuser');
    cy.get('input[name="password"]').type('StrongPassword1!{enter}');
    cy.get('p').should('contain', 'Identifiants ou Mot de passe incorrects.');
  });
});


