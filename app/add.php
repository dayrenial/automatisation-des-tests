<?php
session_start();
require '../db_conn.php';

if(isset($_POST['title']) && !empty($_POST['title']) && isset($_SESSION['user_id'])) {
    $title = trim($_POST['title']);
    $user_id = $_SESSION['user_id']; // Récupération de l'ID utilisateur depuis la session

    // Vérification que la tâche n'existe pas déjà
    $stmt = $conn->prepare("SELECT id FROM todos WHERE title = ? AND user_id = ?");
    $stmt->execute([$title, $user_id]);
    if ($stmt->fetch()) {
        $error = "Une tâche avec ce titre existe déjà.";
        header("Location: ../index.php?mess=error&data=" . urlencode($error));
        exit;
    }

    // Ajout de la tâche
    $stmt = $conn->prepare("INSERT INTO todos(title, user_id) VALUES (?, ?)");
    if ($stmt->execute([$title, $user_id])) {
        header("Location: ../index.php?mess=success");
    } else {
        $error = "Erreur lors de l'ajout de la tâche.";
        header("Location: ../index.php?mess=error&data=" . urlencode($error));
    }
} else {
    $error = "Veuillez entrer un titre pour votre tâche.";
    header("Location: ../index.php?mess=error&data=" . urlencode($error));
    exit;
}
