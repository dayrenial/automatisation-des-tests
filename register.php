<?php 
require 'user_functions.php';

$error_message = '';
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['username'], $_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (!isPasswordStrong($password)) {
        $error_message = "Le mot de passe doit contenir au moins 8 caractères, dont des majuscules, des minuscules, des chiffres et des symboles.";
    } else {
        $result = registerUser($username, $password,$conn);
        if ($result == "success") {
            header("Location: login.php");
            exit;
        } else {
            $error_message = $result;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
    <link rel="stylesheet" href="css/style.css"> <!-- Assurez-vous que le chemin d'accès est correct -->
</head>
<body>
    <div class="main-section">
        <div class="add-section">
            <h1 style="text-align:center; padding: 20px 0;">Inscription</h1>
            <form action="register.php" method="post">
                Nom d'utilisateur: <input type="text" name="username" required>
                Mot de passe: <input type="password" name="password" required>
                <button type="submit">S'inscrire</button>
            </form>
            <?php if (!empty($error_message)) { ?>
                <p style="color:red;"><?php echo $error_message; ?></p>
            <?php } ?>
            <p style="text-align: center;">Déjà inscrit? <a href="login.php">Connectez-vous ici</a>.</p>
        </div>
    </div>
</body>
</html>
