<?php
require 'db_conn.php';
require 'user_functions.php';
session_start();

if (isset($_SESSION['user_id'])) {
    deleteUserAndTasks($_SESSION['user_id'], $conn);
} else {
    header("Location: login.php");
    exit;
}
