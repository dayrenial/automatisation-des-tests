<?php
require_once 'db_conn.php'; 

function isUserAuthenticated() {
    return isset($_SESSION['user_id']);
}

function redirectToLogin() {
    header("Location: login.php");
    exit;
}

function registerUser($username, $password, $conn) {
    $stmt = $conn->prepare("SELECT id FROM users WHERE username = ?");
    $stmt->execute([$username]);
    if ($stmt->fetch()) {
        return "Ce nom d'utilisateur est déjà pris. Veuillez en choisir un autre.";
    }

    $passwordHash = password_hash($password, PASSWORD_DEFAULT);
    $stmt = $conn->prepare("INSERT INTO users (username, password) VALUES (?, ?)");
    if ($stmt->execute([$username, $passwordHash])) {
        return "success";
    } else {
        $errorInfo = $stmt->errorInfo();
        return "Erreur lors de l'inscription: " . $errorInfo[2];
    }
}


function loginUser($username, $password, $conn) {
    $stmt = $conn->prepare("SELECT id, password FROM users WHERE username = ?");
    $stmt->execute([$username]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($user && password_verify($password, $user['password'])) {
        $_SESSION['user_id'] = $user['id'];
        return "success";
    }
    return "Identifiants ou Mot de passe incorrects.";
}


function isPasswordStrong($password) {
    return preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\d]).{8,}$/', $password) > 0;
}

function logoutUser() {

    session_start();
    session_unset();
    session_destroy();
    redirectToLogin();
}



function deleteUserAndTasks($userId, $conn) {

    // D'abord, supprimez toutes les tâches associées à l'utilisateur
    $stmt = $conn->prepare("DELETE FROM todos WHERE user_id = ?");
    $stmt->execute([$userId]);

    // Ensuite, supprimez le compte utilisateur
    $stmt = $conn->prepare("DELETE FROM users WHERE id = ?");
    $stmt->execute([$userId]);
    if (session_status() == PHP_SESSION_ACTIVE) {
        logoutUser();
    }
    
}
